create table course(
    id UUID not null,
    name varchar(50) not null,
    category varchar(50) not null,
    primary key(id)
);


insert  into course values( 'd1a94cb9-fb1c-4769-9c5b-6db28d30814d','kotlin', 'programação');
insert  into course values( '7d2662d0-3519-4db5-a2f2-11b89be8a14a' ,'java', 'programação');
insert  into course values( uuid() ,'html', 'FRONT END');