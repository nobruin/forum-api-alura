create table user (
    id UUID not null,
    name varchar(50) not null,
    email varchar(50) not null,
    primary key(id)
);

insert  into user values('aeebbc96-52be-43fa-8c01-61b9fbca8fd7' ,'bruno.marins', 'nobruin@gmai.com');
insert  into user values('a6e54dad-a5a6-46c6-92b0-d61a78abb142' ,'jessica.marins', 'jessica@gmail.com');
insert  into user values('13710917-7469-4bd7-91cc-af8df36213c9' ,'benjamin.marins', 'jessica@gmail.com');