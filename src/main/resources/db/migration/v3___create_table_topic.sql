create table topic(
    id UUID not null,
    title varchar (50) not null,
    message varchar (50) not null,
    created datetime not null,
    status varchar(50) not null,
    course_id UUID not null,
    author_id UUID not null,
    primary key (id),
    foreign key (course_id) references course(id),
    foreign key (author_id) references user(id)
);