package com.counterbalance.forum.exception

class NotFoundException(message: String?): RuntimeException(message)