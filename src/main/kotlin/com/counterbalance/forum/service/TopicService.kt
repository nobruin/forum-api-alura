package com.counterbalance.forum.service

import com.counterbalance.forum.dto.CategoryDto
import com.counterbalance.forum.dto.TopicForm
import com.counterbalance.forum.dto.TopicUpdateForm
import com.counterbalance.forum.dto.TopicView
import com.counterbalance.forum.exception.NotFoundException
import com.counterbalance.forum.mapper.FormTopicMapper
import com.counterbalance.forum.mapper.TopicViewMapper
import com.counterbalance.forum.repository.TopicRepository
import org.springframework.beans.factory.annotation.Value
import org.springframework.dao.EmptyResultDataAccessException
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service
import java.util.*

@Service
class TopicService(
    private val repository: TopicRepository,
    private val viewMapper: TopicViewMapper,
    private val topicMapper: FormTopicMapper,
    @Value("Topic not Found!")
    private val notFoundMessage: String
) {
    fun list(
        courseName: String?,
        pageable: Pageable
    ): Page<TopicView> {
        val topics = if (courseName == null){
            repository.findAll(pageable)
        }else{
            repository.findByCourseName(courseName, pageable)
        }

        return topics.map(viewMapper::map)
    }

    fun getById(id: UUID): TopicView {
        val topic = repository.findById(id).orElseThrow{ throw NotFoundException(notFoundMessage)}
        return viewMapper.map(topic)
    }

    fun create(topicForm: TopicForm): TopicView {
        return topicMapper.map(topicForm).let { topic ->
            repository.save(topic)
            viewMapper.map(topic)
        }
    }

    fun update(topicUpdate: TopicUpdateForm): TopicView {
        val topic = repository.findById(topicUpdate.id).orElseThrow{throw NotFoundException(notFoundMessage)}
        topic.title = topicUpdate.title
        topic.message = topicUpdate.message

        return viewMapper.map(topic)
    }

    fun delete(id: UUID) {
        try {
            repository.deleteById(id)
        }catch (ex: EmptyResultDataAccessException){
            throw NotFoundException(notFoundMessage)
        }
    }

    fun categories(): List<CategoryDto>  = repository.reportCategories()
}

