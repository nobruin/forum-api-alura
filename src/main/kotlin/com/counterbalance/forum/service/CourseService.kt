package com.counterbalance.forum.service

import com.counterbalance.forum.model.Course
import com.counterbalance.forum.repository.CourseRepository
import org.springframework.stereotype.Service
import java.util.*

@Service
class CourseService(private val repository: CourseRepository) {


    fun list(): List<Course> = repository.findAll()

    fun getById(id: UUID): Course = repository.getById(id)
}
