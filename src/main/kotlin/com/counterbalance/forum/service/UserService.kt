package com.counterbalance.forum.service

import com.counterbalance.forum.model.User
import com.counterbalance.forum.repository.UserRepository
import org.springframework.stereotype.Service
import java.util.*

@Service
class UserService(private val repository: UserRepository) {

    fun list(): List<User> {
        return repository.findAll()
    }

    fun getById(id: UUID): User = repository.getById(id)
}
