package com.counterbalance.forum.mapper

import com.counterbalance.forum.dto.TopicForm
import com.counterbalance.forum.model.Topic
import com.counterbalance.forum.service.CourseService
import com.counterbalance.forum.service.UserService
import org.springframework.stereotype.Component
import java.util.*

@Component
class FormTopicMapper(
    private val courseService: CourseService,
    private val userService: UserService,
) : Mapper<TopicForm, Topic> {
    override fun map(t: TopicForm): Topic = t.let { topicForm ->
        return Topic(
            id = UUID.randomUUID(),
            title = topicForm.title,
            message = topicForm.message,
            author = userService.getById(topicForm.idAuthor),
            course = courseService.getById(topicForm.idCourse)
        )
    }
}