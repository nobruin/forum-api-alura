package com.counterbalance.forum.mapper

import com.counterbalance.forum.dto.TopicView
import com.counterbalance.forum.model.Topic
import org.springframework.stereotype.Component

@Component
class TopicViewMapper: Mapper<Topic, TopicView> {

    override fun map(t: Topic): TopicView = t.let{
        TopicView(
            id = it.id,
            title = it.title,
            message = it.message,
            created = it.created,
            status = it.status
        )
    }
}