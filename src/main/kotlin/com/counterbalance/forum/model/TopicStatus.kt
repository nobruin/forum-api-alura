package com.counterbalance.forum.model

enum class TopicStatus {
    NOT_ANSWERED,
    NOT_SOLVED,
    SOLVED,
    CLOSED
}
