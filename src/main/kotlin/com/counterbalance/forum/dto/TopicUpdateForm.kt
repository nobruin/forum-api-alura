package com.counterbalance.forum.dto

import java.util.*
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.NotNull
import javax.validation.constraints.Size

data class TopicUpdateForm (
    @field:NotNull
    val id: UUID,
    @field:NotEmpty
    @field:Size(min=5, max = 150)
    val title: String,
    @field:NotEmpty
    @field:Size(min=5, max = 4500)
    val message: String
)