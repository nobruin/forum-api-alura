package com.counterbalance.forum.dto

data class CategoryDto(
    val category: String,
    val count: Long
)
