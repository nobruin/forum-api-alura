package com.counterbalance.forum.dto

import com.counterbalance.forum.model.TopicStatus
import java.time.LocalDateTime
import java.util.*

data class TopicView(
    val id: UUID?,
    val title: String,
    val message: String,
    val status: TopicStatus,
    val created: LocalDateTime
)