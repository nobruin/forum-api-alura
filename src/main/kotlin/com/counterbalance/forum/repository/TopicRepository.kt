package com.counterbalance.forum.repository

import com.counterbalance.forum.dto.CategoryDto
import com.counterbalance.forum.model.Topic
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import java.util.*

interface TopicRepository : JpaRepository<Topic, UUID> {

    fun findByCourseName(nameCourse: String?, pageable: Pageable): Page<Topic>

    @Query("SELECT new com.counterbalance.forum.dto.CategoryDto (course.category, count(t)) From Topic t JOIN t.course course group by course.category")
    fun reportCategories(): List<CategoryDto>


}
