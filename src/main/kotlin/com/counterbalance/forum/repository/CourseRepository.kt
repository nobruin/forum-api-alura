package com.counterbalance.forum.repository

import com.counterbalance.forum.model.Course
import org.springframework.data.jpa.repository.JpaRepository
import java.util.*

interface CourseRepository: JpaRepository<Course,UUID >