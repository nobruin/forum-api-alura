package com.counterbalance.forum.repository

import com.counterbalance.forum.model.User
import org.springframework.data.jpa.repository.JpaRepository
import java.util.*

interface UserRepository: JpaRepository<User, UUID>