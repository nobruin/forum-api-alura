package com.counterbalance.forum.controller

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/hello1")
class HelloController {

    @GetMapping
    fun getHello(): String {
        return "Hello world 1!!!"
    }
}