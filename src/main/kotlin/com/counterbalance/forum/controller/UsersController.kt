package com.counterbalance.forum.controller

import com.counterbalance.forum.service.UserService
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import com.counterbalance.forum.model.User
import org.springframework.web.bind.annotation.GetMapping

@RestController
@RequestMapping("/users")
class UsersController(private val service: UserService) {

    @GetMapping
    fun list(): List<User> = service.list()
}