package com.counterbalance.forum.controller

import com.counterbalance.forum.dto.CategoryDto
import com.counterbalance.forum.dto.TopicForm
import com.counterbalance.forum.dto.TopicUpdateForm
import com.counterbalance.forum.dto.TopicView
import com.counterbalance.forum.service.TopicService
import org.springframework.cache.annotation.CacheEvict
import org.springframework.cache.annotation.Cacheable
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.domain.Sort
import org.springframework.data.web.PageableDefault
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.bind.annotation.*
import org.springframework.web.util.UriComponentsBuilder
import java.util.*
import javax.validation.Valid

@RestController
@RequestMapping("/topics")
class TopicController(private val service: TopicService) {
    @GetMapping
    @Cacheable(value = ["Topics"])
    @ResponseStatus(HttpStatus.PARTIAL_CONTENT)
    fun list(
        @RequestParam(required = false) courseName: String?,
        @PageableDefault(size = 20, sort = ["created"], direction = Sort.Direction.DESC)
        pagination: Pageable
    ): Page<TopicView> = service.list(courseName, pagination)

    @GetMapping("/{id}")
    fun get(@PathVariable id: UUID): TopicView = service.getById(id)

    @PostMapping
    @CacheEvict(value = ["Topics"], allEntries = true)
    @Transactional
    @ResponseStatus(HttpStatus.CREATED)
    fun create(@RequestBody @Valid topicForm: TopicForm, componentsBuilder: UriComponentsBuilder): ResponseEntity<TopicView>{
        val topicView = service.create(topicForm)
        val uri = componentsBuilder.path("/topics/${topicView.id}").build().toUri()
        return ResponseEntity.created(uri).body(topicView)
    }

    @PutMapping
    @CacheEvict(value = ["Topics"], allEntries = true)
    fun update(@RequestBody @Valid topicForm: TopicUpdateForm):ResponseEntity<TopicView> {
        val topicView = service.update(topicForm)
        return ResponseEntity.ok(topicView)
    }

    @DeleteMapping("/{id}")
    @Transactional
    @CacheEvict(value = ["Topics"], allEntries = true)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun delete(@PathVariable id: UUID) = service.delete(id)

    @GetMapping("/categories")
    fun categories(): List<CategoryDto> = service.categories()
}