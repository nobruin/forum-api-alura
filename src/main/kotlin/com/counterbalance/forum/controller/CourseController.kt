package com.counterbalance.forum.controller

import com.counterbalance.forum.model.Course
import com.counterbalance.forum.service.CourseService
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.util.*

@RestController
@RequestMapping("/courses")
class CourseController(private val service: CourseService) {
    @GetMapping
    fun list(): List<Course> = service.list()

    @GetMapping("/{id}")
    fun get(@PathVariable id: UUID): Course = service.getById(id)
}