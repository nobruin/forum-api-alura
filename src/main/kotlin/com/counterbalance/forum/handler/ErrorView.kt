package com.counterbalance.forum.handler

import java.time.LocalDateTime

data class ErrorView(
    val statusCode: Int,
    val error: String?,
    val path: String
)
