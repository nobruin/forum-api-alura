package com.counterbalance.forum.handler

import com.counterbalance.forum.exception.NotFoundException
import org.springframework.http.HttpStatus
import org.springframework.web.bind.MethodArgumentNotValidException
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestControllerAdvice
import java.lang.Exception
import javax.servlet.http.HttpServletRequest

@RestControllerAdvice
class ExceptionHandler {

    @ExceptionHandler(NotFoundException::class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    fun handleNotFound(
        exception: NotFoundException,
        request: HttpServletRequest
    ): ErrorView = ErrorView(
        statusCode = HttpStatus.NOT_FOUND.value(),
        error = exception.message,
        path = request.servletPath
    )

    @ExceptionHandler(Exception::class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    fun handleServerError(
        exception: Exception,
        request: HttpServletRequest
    ): ErrorView = ErrorView(
        statusCode = HttpStatus.INTERNAL_SERVER_ERROR.value(),
        error = exception.message,
        path = request.servletPath
    )

    @ExceptionHandler(MethodArgumentNotValidException::class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    fun handleValidationError(
        exception: MethodArgumentNotValidException,
        request: HttpServletRequest
    ): ErrorView {
        val errorMessage = HashMap<String, String?>()
        exception.bindingResult.fieldErrors.map {
            e -> errorMessage.put(e.field, e.defaultMessage)
        }
        return ErrorView(
            statusCode = HttpStatus.BAD_REQUEST.value(),
            error = errorMessage.toString(),
            path = request.servletPath
        )
    }
}